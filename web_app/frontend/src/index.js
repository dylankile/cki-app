import React from 'react'; // Necessary when using JSX the html style code
import ReactDOM from 'react-dom'; // Renders things to a page
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
// Render requires html and puts the html code in 'root' element 
// found in index.html

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
