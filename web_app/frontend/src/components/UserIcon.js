import React from 'react'
import '../styles/userIcon.css'

class UserIcon extends React.Component {
	constructor(props) {
		super(props) 
		this.state = {
			hover: false
		}

		this.onHoverHandler = this.onHoverHandler.bind(this)
		this.onMouseLeaveHandler = this.onMouseLeaveHandler.bind(this)
	}
	onHoverHandler(e) {
		this.setState({
			hover: true
		})
	}
	onMouseLeaveHandler(e) {
		this.setState({
			hover: false
		})
	}
	render() {
		const user = this.props.content 
		const firstNameLetter = user["first name"].charAt(0) 
		const lastNameLetter = user["last name"].charAt(0)
		const initials = firstNameLetter + lastNameLetter
		return (
			<div>
				<div className='user-icon' onMouseEnter={this.onHoverHandler}
					onMouseLeave={this.onMouseLeaveHandler}> 
					<p>{initials}</p>
				</div>
				<div className='user-name-tag'> 
					{this.state.hover && <p>{user['first name']} {user['last name']}</p>}
				</div>
			</div>
		)
	}
}

export default UserIcon