import React from 'react'

class ContactCard extends React.Component {
	constructor() {
		super()
		this.state = {
			imgURL: null
		}
	}

	handleHover() {
		console.log("Hello")
	}
	render() {
		let contact = this.props.contact
		return (
			<div className='contact-card'> 
				<img onMouseOver={() => console.log("Hello")} src={contact.imgURL}/>
				<h3>{contact.name}</h3> 
				<p>Phone: {contact.phone}</p> 
				<p>Email: {contact.email}</p>
			</div>	
		)
	}
}

export default ContactCard