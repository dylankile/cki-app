import React from 'react'
import '../styles/events.css'
import { withRouter } from "react-router"
import PropTypes from 'prop-types'

class AddEventButton extends React.Component {
	static propTypes = {
		history: PropTypes.object.isRequired,
	}
	constructor(props) {
		super(props)

		this.handleClick = this.handleClick.bind(this)
	}

	handleClick(e) {
		this.props.history.push('/add-event')
	}

	render() {
		return (
			<div className = 'add-event-button' onClick={this.handleClick}> 
				<p>Add Event</p>
			</div>
		)
	}
}

export default withRouter(AddEventButton)