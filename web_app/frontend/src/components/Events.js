import React from 'react'
import EventCard from './EventCard'
import AddEventButton from './AddEventButton'
import '../styles/events.css'

class Events extends React.Component {

	constructor() {
		super() 
		this.state = {
			loading: true,
			events: null
		}
	}

	componentDidMount() {
		const url = "http://localhost:5000/events"
		fetch(url) 
			.then(response => response.json())
			.then(data => {
				this.setState({
					loading: false,
					events: data 
				})
			})
	}

	render() {
		console.log(this.state.events)
		const eventComponents = this.state.events ? 
			this.state.events.map(event => < EventCard key={event.id} content={event}/>) : 
			null
		return (
			<div className='events'> 
				<div className = 'cards'>
					{this.state.loading ? 'Loading...' : eventComponents}
				</div>
				<div> 
					<AddEventButton />
				</div>
			</div>
		)
	}
}

export default Events


