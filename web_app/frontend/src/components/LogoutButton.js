import React from 'react' 

class LogoutButton extends React.Component {
	constructor(props) {
		super(props) 

	}

	render() {
		return (
			<div onClick={this.props.logout}> 
				<p>Logout</p>
			</div>
		)
	}
}

export default LogoutButton