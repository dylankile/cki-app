import React from 'react' 
import '../styles/navbarDropdown.css'
import LogoutButton from './LogoutButton'

class NavbarUserIcon extends React.Component {
	render() {
		return (
			<div className='nav-dropdown'>
				<div className='nav-user-icon'> 
					<p className='user'>{this.props.user["first name"]}</p>
				</div>
				<div className='nav-dropdown-content'> 
					<LogoutButton logout={this.props.logout}/>
				</div>			
			</div>
		)
	}
}

export default NavbarUserIcon 