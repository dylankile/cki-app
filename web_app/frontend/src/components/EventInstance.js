import React from 'react' 
import App from "../App"
import JoinEventButton from './JoinEventButton'
import UserIcon from './UserIcon.js'
import '../styles/eventInstance.css'
import '../styles/userIcon.css'


class EventInstance extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			name: '',
			date: '', 
			start: '', 
			end: '', 
			address: '', 
			description: '', 
			type: '', 
			url: '',
			users: []
		}
		this.updateState = this.updateState.bind(this)
		this.checkIfUserHasntJoined = this.checkIfUserHasntJoined.bind(this)
	}

	checkIfUserHasntJoined(username) {
		for (let user of this.state.users) {
			if (user.username === username) { 
				console.log("false")
				return false
			}
		}
		console.log("true")
		return true
	}

	updateState(data) {
		this.setState(prevState => {
			users: prevState.users.append(data) 
		})
	}

	componentDidMount() {

		const id = this.props.match.params.id
		fetch(App.db_url + "event/" + id)
			.then(response => response.json())
			.then(data => {
				this.setState({
					name: data['name'],
					date: data['date'], 
					start: data['start'], 
					end: data['end'], 
					address: data['address'], 
					description: data['description'], 
					type: data['type'], 
					url: data['url'], 
					users: data['users']
				})
				console.log(this.state.users)
			})

	}
	render() {
		const userIconComponents = this.state.users ? 
			this.state.users.map(user => <UserIcon key={user.username} content={user}/>) : 
			null
		return (
			<div className='event-instance'>
				<div>
					<h1>{this.state.name}</h1>
					<p>Type of event: {this.state.type}</p>
					<p>Date: {this.state.date}</p> 
					<p>Time: {this.state.start} to {this.state.end}</p>
					<p>Address: {this.state.address}</p> 
					<p>URL: {this.state.url}</p>
					<p>Description: {this.state.description}</p>
					{this.state.users && 
					<div className='user-icon-container'>
						{userIconComponents}
					</div>}
				</div>
				{this.props.user && this.checkIfUserHasntJoined(this.props.user.username) ?
				<div className='join-event'> 
					<JoinEventButton user={this.props.user} eid={this.props.match.params.id}
						handler={this.updateState}/>
				</div> : 
				<div> 
					
				</div>}
			</div>
		)
	}
}

export default EventInstance