import React from 'react'
import '../styles/eventCard.css'
import { withRouter } from "react-router"
import PropTypes from 'prop-types'


class EventCard extends React.Component {	
	static propTypes = {
		history: PropTypes.object.isRequired
	}
	constructor() {
		super() 

		this.clickHandler = this.clickHandler.bind(this)
	}
	clickHandler(e) {
		this.props.history.push('/event/' + this.props.content.id)
	} 
	render() {
		const event = this.props.content
		return (
			<div className='event-card' onClick={this.clickHandler} className='event-card'>
				<div className='container'>
					<h3>{event.name}</h3> 
					<p>{event.date}</p> 
					<p>{event.start} to {event.end}</p>
				</div>
			</div>
		)
	}
}

export default withRouter(EventCard)