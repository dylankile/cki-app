import React from 'react' 
import App from '../App'
import { withRouter } from "react-router"
import PropTypes from 'prop-types'

class Register extends React.Component {
	static propTypes = {
		history: PropTypes.object.isRequired
	}
	constructor(props) {
		super(props)
		this.state = {
			f_name : "", 
			l_name : "", 
			username : "", 
			password : ""
		}
	 this.onChangeHandler = this.onChangeHandler.bind(this);
	 this.submitHandler = this.submitHandler.bind(this);
	}
	onChangeHandler(e) {
		const target = e.target 
		const name = target.name 
		const value = target.value 
		this.setState({
			[name] : value
		}) 
	}
	submitHandler(e) {
		// Do password checking requirements here
		e.preventDefault()
		fetch(App.db_url + "add_user", {
			method: 'POST', 
			headers: {
			    'Accept': 'application/json',
			    'Content-Type': 'application/json',
			    'Access-Control-Request-Method': 'POST'
  			},
  			body: JSON.stringify({
  				user_name: this.state.username, 
  				password: this.state.password,
  				first_name: this.state.f_name, 
  				last_name: this.state.l_name
  			})
		})
  			.then(response => response.json())
  			.then(data => {
  				const responseValid = data["username"]
  				console.log(data)
  				if (responseValid) {
  					this.props.history.push('/events')
  					this.props.handler(data)
  				}
  			})
	}
	render() {
		return (
			<form onSubmit={this.submitHandler}>
				<p>First Name: </p>
				<input type="text" name="f_name" placeholder="First Name" 
					value={this.state.f_name} onChange={this.onChangeHandler}/>
				<p>Last Name: </p>
				<input type="text" name="l_name" placeholder="Last Name" 
					value={this.state.l_name} onChange={this.onChangeHandler}/>
				<p>Username: </p>
				<input type="text" name="username" placeholder="Username" 
					value={this.state.username} onChange={this.onChangeHandler}/>
				<p>Password: </p>
				<input type="password" name="password" value={this.state.password}
					onChange={this.onChangeHandler}/>
				<input type="submit" text="Register"/>

			</form>
		) 
	}
}

export default withRouter(Register)