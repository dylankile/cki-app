import React from 'react'
import App from '../App'
import { withRouter } from "react-router"
import PropTypes from 'prop-types'

class Login extends React.Component {
	static propTypes = {
		history: PropTypes.object.isRequired,
	}
	constructor(props) {
		super(props)
		this.state = {
			username: '', 
			password: '',
		}
		this.login = this.login.bind(this)
		this.changeHandler = this.changeHandler.bind(this)
		this.submitHandler = this.submitHandler.bind(this)
	}

	changeHandler(e) {
		const name = e.target.name 
		const val = e.target.value
		this.setState({
			[name] : val
		})

	}
	submitHandler(e) {
		e.preventDefault()
		fetch(App.db_url + "verify_user", {
			method: 'POST', 
			headers: {
			    'Accept': 'application/json',
			    'Content-Type': 'application/json',
			    'Access-Control-Request-Method': 'POST'
  			},
  			body: JSON.stringify({
  				username: this.state.username, 
  				password: this.state.password
  			})
  		})
  			.then(response => response.json())
  			.then(data => {
  				const responseValid = data["username"]
  				if (responseValid) {
  					this.props.history.push('/events')
  					this.props.handler(data)
  				}
  			})

	}

	login() {
		return (
			<form onSubmit={this.submitHandler}>
				<div>
					<p>Username: </p>
					<input type='text' name='username' placeholder='Username' value={this.state.username} onChange={this.changeHandler}/> 
					<p>Password: </p>
					<input type='password' name='password' value={this.state.password} onChange={this.changeHandler}/>
					<input type='submit' text='Log in' />
				</div>
			</form>
		)
	}
	render() {
		// const { history } = this.props
		return (
			<div> 
				{this.props.user ? <p>'User logged in'</p> : this.login()}
			</div>
		)
	}
}

export default withRouter(Login)