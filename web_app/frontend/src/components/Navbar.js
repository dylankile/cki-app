import React from 'react'
import '../styles/navbar.css'
import NavbarButton from './NavbarButton'
import NavbarUserIcon from './NavbarUserIcon'
import LoginButton from './LoginButton'


class Navbar extends React.Component {
	render() {
		const user = this.props.user
		return (
			<header className="navbar"> 
				<NavbarButton text="Home"/> 
				<NavbarButton text="Events" url='/events' />
				{user ? <NavbarUserIcon user={user} logout={this.props.logout}/> : 
					    <LoginButton />}
			</header>
		)
	}
}

export default Navbar