import React from 'react'
import { withRouter } from "react-router"
import PropTypes from 'prop-types'
import '../styles/loginButton.css'

class LoginButton extends React.Component {
	static propTypes = {
		history: PropTypes.object.isRequired,
	}
	constructor(props) {
		super(props) 

		this.handleClick = this.handleClick.bind(this)
	}

	handleClick(e) {
		this.props.history.push('/login')
	}

	render() {
		return (
			<div className='login-button' onClick={this.handleClick}> 
				<p>Sign in</p>
			</div>
		)
	}
}

export default withRouter(LoginButton)