import React from 'react'

class UserCard extends React.Component {
	constructor() {
		super()
	}

	render() {
		let user = this.props.user
		console.log(user)
		return (
			<div className='contact-card'> 
				<h3>Name: {user['first name']} {user['last name']}</h3> 
				<p>Username: {user.username}</p> 
			</div>	
		)
	}
}

export default UserCard