import React from 'react'
import '../styles/addEvent.css'
import TimePicker from './TimePicker'
import App from '../App'

class AddEvent extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			name: '',
			description: 'Please write a description about the event.',
			type: 'Social',
			startHr: '1',
			startMin: '0',
			startPeriod: 'am',
			endHr: '1',
			endMin: '0',
			endPeriod: 'am',
			url: '',
			location: '', 
			date: null 
		}
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleChange = this.handleChange.bind(this)
	}

	handleSubmit(e) {
		const state = this.state
		console.log(App.db_url+"add_event")
		fetch(App.db_url+"add_event", {
			method: 'POST', 
			headers: {
			    'Accept': 'application/json',
			    'Content-Type': 'application/json',
			    'Access-Control-Request-Method': 'POST'
  			},
			body: JSON.stringify({
				name: state.name, 
				type: state.type, 
				description: state.description, 
				start_time: `${state.startHr}:${state.startMin} ${state.startPeriod}`, 
				end_time: `${state.endHr}:${state.endMin} ${state.endPeriod}`,
				date: state.date,
				address: state.location, 
				url: state.url
			})
		})
		.then(response => {
			this.props.history.push('/events')	
		})
		e.preventDefault() 
		

	}

	handleChange(event) {
		const target = event.target 
		const value = target.value
		const name = target.name
		console.log('Changed!', value)
		this.setState({
			[name]: value
		})
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit}> 
				<input name='name' type='text' placeholder='Name of Event' onChange={this.handleChange}/>
				<select name= 'type' onChange={this.handleChange}>
					<option value='Social'>Social</option> 
					<option value='Service'>Service</option>
				</select>
				<input name='date' type='date' onChange={this.handleChange}/>
				<TimePicker name='start' onChangeHandler={this.handleChange}/> 
				<TimePicker name='end' onChangeHandler={this.handleChange}/>
				<input type='text' name='location' placeholder="Location of Event" onChange={this.handleChange}/>
				<textarea name='description' value={this.state.description} onChange={this.handleChange}/>
				<input type='text' name='url' placeholder="Enter sign-up url (Optional)" onChange={this.handleChange}/>
				<input type='submit' text='Submit'/>
			</form>
		)
	}
}

export default AddEvent