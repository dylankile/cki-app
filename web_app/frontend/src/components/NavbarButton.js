import React from 'react' 
import { withRouter } from "react-router"
import PropTypes from 'prop-types'

class NavbarButton extends React.Component {
	constructor(props) {
		super(props)

		this.clickHandler = this.clickHandler.bind(this)
	}

	clickHandler(e) {
		this.props.history.push(this.props.url)
	}

	render() {
		return (
			<div onClick={this.clickHandler} className="nav-button">
				<p>{this.props.text}</p>
			</div>
		)
	}
}

export default withRouter(NavbarButton)