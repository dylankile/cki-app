import React from 'react'

function createSelectItems(start, end, step=1) {
	let items = [] 
	for (let i = start; i <= end; i+=step) {
		items.push(<option key={i} value={i}>{i}</option>)
	}
	return items
}
function TimePicker(props) {

	return (
		<div>
			<select name={`${props.name}Hr`} onChange={props.onChangeHandler}>{createSelectItems(1,12)}</select>
			<select name={`${props.name}Min`} onChange={props.onChangeHandler}>{createSelectItems(0,59,15)}</select>
			<select name={`${props.name}Period`} onChange={props.onChangeHandler}>
				<option value='am'>AM</option> 
				<option value='pm'>PM</option>
			</select>
		</div>
	)
}

export default TimePicker