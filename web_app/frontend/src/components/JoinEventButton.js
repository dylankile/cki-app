import React from 'react' 
import App from '../App'

class JoinEventButton extends React.Component {
	constructor(props) {
		super(props)
		console.log("Created button")
		this.handleClick = this.handleClick.bind(this)
	}
	handleClick(e) {
		const props = this.props
		fetch(App.db_url + "join_event", {
			method: 'POST', 
			headers: {
			    'Accept': 'application/json',
			    'Content-Type': 'application/json',
			    'Access-Control-Request-Method': 'POST'
				},
				body: JSON.stringify({
					username: props.user.username, 
					eid: props.eid
				})
		})
		.then(response => {
			window.location.reload()
		})

	}
	render() {
		return (
			<div onClick={this.handleClick}>
				<p>Join Event</p>
			</div> 
		)
	}
}

export default JoinEventButton