import React from 'react'
import '../App.css' 
import UserCard from './UserCard'

// Curly brace means enter javascript land

class MainContent extends React.Component {
	constructor() {
		super() 
		this.state = {
			loading: true,
		 	users: null
		}
	}

	/** 				   Lifecycle methods		
	  *	 
	  *	getDerivedStateFromProps(), getSnapshotBeforeUpdate() 
	  * componentDidMount(), shouldComponentUpdate(), 
	  * componentWillUnmount(), render()
	  *
	  */
	// static getDerivedStateFromProps(props, state) {
	// 	// return new, updated state based upon the props
	// 	// probably don't need it
	// }

	// getSnapshotBeforeUpdate() {
	// 	// create a backup of the current way things are
	// }

	componentDidMount() {
		const url = "http://localhost:5000/users"
		fetch(url)
			.then(response => response.json()) 
			.then(data => {
				this.setState({
					users: data,					
					loading: false
				})
			})
		// only run once, render doesn't cause it to run more
		// useful for API calls
		// Can call setTimeout() to halt webpage loading

	}

	// shouldComponentUpdate(nextProps, nextState) {
	// 	// return true if want it to update or false if not
	// }

	// componentWillUnmount() {
	// 	// teardown or cleanup your code before component disappears
	// 	// useful to remove say an event listener you set up in compdidmount 
	// }

	// arrow function 
	// if one parameter don't need parenthesis
	// don't need curly braces or return statement since it's the only thing in the function
	// essentially maps each joke object in the data to a Joke component
	render() {
		console.log("render",this.state.users)
		const userComponents = this.state.users ?
			this.state.users.map(user => < UserCard key={user.id} user={user}  />) : 
			null
		// return can take an array of components. thas cool
		return ( 
			<main className="contact-card">
				{this.state.loading ? 'Loading' : userComponents}
			</main>
		)
	}
}

export default MainContent