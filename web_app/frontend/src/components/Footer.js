import React from "react"
import '../styles/footer.css'

class Footer extends React.Component {
	render() {
		return (
			<footer className='footer'> 
				<h3>Created by Dylan Kile</h3> 
			</footer>
		)
	}
}

export default Footer