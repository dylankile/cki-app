import React from 'react'
import Footer from './components/Footer'
import Navbar from './components/Navbar'
import AddEvent from './components/AddEvent'
import Events from './components/Events'
import Login from './components/Login'
import Register from './components/Register'
import EventInstance from './components/EventInstance'
import {BrowserRouter, Route, Switch, withRouter} from 'react-router-dom'
import {instanceOf} from 'prop-types'
import {withCookies, Cookies, CookiesProvider} from 'react-cookie'
import "./styles/app.css"

// Use camel case and uppercase Letter for components
// This is a functional component
// function App() {
//   return (
//     <div className="App">
//       <Navbar />
//       <MainContent />
//       <Footer />
//     </div>
//   );
// }

// with a class based component, you need this so {this.props....}
class App extends React.Component {
  static db_url = 'http://localhost:5000/'
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  }
  
  constructor(props) { //is the constructor method for initialization
    super(props) // always add super if it extends

    const {cookies} = props
    this.state = {
      user: cookies.get('user')
    }
    this.loginHandler = this.loginHandler.bind(this)
    this.logoutHandler = this.logoutHandler.bind(this)
  }

  loginHandler(user) {
    const {cookies} = this.props;
    cookies.set('user', user)
    this.setState({
      user: user
    })
    console.log(cookies.get('user'))
  }
  logoutHandler() {
    console.log("We here")
    const {cookies} = this.props;
    cookies.remove('user') 
    this.setState({
      user: null
    })
  }
  render() {
    return ( 
      <div>
        <CookiesProvider>
          <BrowserRouter>
            <div>
              <Navbar user={this.state.user} logout={this.logoutHandler}/>
              <Switch>
                <Route path='/login' exact render={() =>
                  <Login user={this.state.user} handler={this.loginHandler} />} />
                <Route path='/register' exact render={() =>
                  <Register user={this.state.user} handler={this.loginHandler} />} />
                <Route path='/events' exact component={Events} />
                <Route path='/add-event' exact component={AddEvent} />
                <Route path = '/event/:id' exact render={(props) =>
                  <EventInstance user={this.state.user} {...props}/>} />
              </Switch>
            </div>
          </BrowserRouter>
          <Footer />
        </CookiesProvider>
      </div>
    )
  }
}

export default withCookies(App);

