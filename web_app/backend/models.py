from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__) 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////mnt/c/Users/dylan/Documents/Projects/cki-app/web_app/backend/ckidb.db'
db = SQLAlchemy(app)


attendees = db.Table('attendees', 
	db.Column('user_id',  db.Integer, db.ForeignKey('user.uid'),  primary_key=True), 
	db.Column('event_id', db.Integer, db.ForeignKey('event.eid'), primary_key=True)
)

class User(db.Model): 
	uid = db.Column(db.Integer(), primary_key = True) # Also a foreign key
	admin_rights = db.Column(db.Boolean(), default=False)
	user_name = db.Column(db.String(30), unique=True) 
	first_name = db.Column(db.String(20))
	last_name = db.Column(db.String(20))
	password = db.Column(db.String(35)) 
	events = db.relationship('Event', secondary=attendees, lazy='dynamic',
		backref=db.backref('users', lazy='dynamic'))

	def __repr__(self): 
		return '<User %s>' % self.user_name

	def toDict(self): 
		return {
			"id": self.uid, 
			"username": self.user_name, 
			"admin": self.admin_rights,
			"first name": self.first_name, 
			"last name": self.last_name
		}

class Event(db.Model): 
	eid = db.Column(db.Integer(), primary_key = True) # Also a foreign key
	name = db.Column(db.String(35))
	date = db.Column(db.String(20))
	start = db.Column(db.String(10))
	end = db.Column(db.String(10)) 
	addr = db.Column(db.String(35))
	description = db.Column(db.String(1000)) 
	type_of_event = db.Column(db.String(8)) 
	sign_in_url = db.Column(db.String(35))


	def __repr__(self): 
		return '<Event %s>' % self.name

	def toDict(self): 
		userObjs = self.users.all()
		users = []
		for user in userObjs: 
			userDict = {}
			userDict["username"] = user.user_name 
			userDict["first name"] = user.first_name
			userDict["last name"] = user.last_name			
			users.append(userDict)
		return {
			"id": self.eid,
			"name": self.name,
			"date": self.date, 
			"start": self.start, 
			"end": self.end, 
			"address": self.addr, 
			"description": self.description, 
			"type": self.type_of_event, 
			"url": self.sign_in_url, 
			"users": users
		}


