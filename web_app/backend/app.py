from flask import Flask, request, jsonify, abort
from flask_cors import CORS, cross_origin
from flask_sqlalchemy import SQLAlchemy
from models import User, Event, app, db
import json
import requests

cors = CORS(app)

@app.route('/') 
def default_page(): 
	return "Always serving"

@app.route('/send_user_post')
def send_user_post(): 
	user_json = { 
				"user_name":"wantapie", 
				"first_name":"Dylan", 
				"last_name":"Kile", 
				"password":"password"
				}
	res = requests.post("http://localhost:5000/add_user", json=user_json) 
	if res.ok: 
		return "Success"
	return "Bad Response"
	
################
# GET REQUESTS #
################

@app.route('/users') 
@cross_origin()
def getUsers(): 
	ret = []
	users = User.query.all() 
	for user in users: 
		ret.append(user.toDict())
		
	return jsonify(ret)

@app.route('/user/<name>') 
@cross_origin() 
def getUserByUsername(name): 
	ret = []
	user = User.query.filter_by(user_name=name).first() 
	if user is not None: 
		print(user)
		ret = user.toDict()
	return jsonify(ret)

@app.route('/events')
@cross_origin() 
def getEvents(): 
	ret = []
	events = Event.query.all() 
	for event in events: 
		ret.append(event.toDict())
	return jsonify(ret)

@app.route('/event/<id>')
@cross_origin() 
def getEventByID(id): 
	ret = []
	event = Event.query.get(id) 
	if (event is not None): 
		ret = event.toDict()
	return jsonify(ret)

#################
# POST REQUESTS #
################# 

@app.route('/add_user', methods=['POST'])
@cross_origin()
def addUser(): 
	ret = []
	content = request.json
	usr = User(user_name  = content["user_name"],
			   first_name = content["first_name"],
			   last_name  = content["last_name"],
			   password   = content["password"])
	user = User.query.filter_by(user_name=usr.user_name).first()
	if user is None:
		print("Hello")
		db.session.add(usr) 
		db.session.commit()
		user = User.query.filter_by(user_name=usr.user_name).first()
		ret = user.toDict()
	return jsonify(ret)

@app.route('/verify_user', methods=['POST'])
@cross_origin()
def verifyUser(): 
	ret = []
	content = request.json 
	name = content["username"]
	password = content["password"] 
	user = User.query.filter(User.user_name == name, User.password == password).first()
	if user is not None: 
		ret = user.toDict()
	return jsonify(ret)



@app.route('/add_event', methods=['POST'])
@cross_origin()
def addEvent():
	content = request.json
	event = Event(name = content["name"],
				  start = content["start_time"],
				  end = content["end_time"], 
				  date = content["date"],
				  addr = content["address"],
	 			  description = content["description"],
				  type_of_event = content["type"], 
				  sign_in_url = content["url"])
	db.session.add(event) 
	db.session.commit()
	return ""

@app.route('/join_event', methods=['POST'])
@cross_origin()
def joinEvent(): 
	print("joining an event")
	content = request.json
	username = content["username"]
	user = User.query.filter_by(user_name=username).first()
	event = Event.query.get(content["eid"])
	assert(user is not None) 
	assert(event is not None)
	user.events.append(event)
	db.session.commit()
	return "Joined"



###################
# DELETE REQUESTS #
###################

@app.route('/delete_user')
def deleteUser(): 
	uid = request.args.get('uid') 
	if uid is None: 
		abort(404)
	row = User.query.get(uid) 
	if row is not None: 
		db.session.delete(row) 
		db.session.commit()
	return "Deleted"
	 
@app.route('/delete_event') 
def deleteEvent(): 
	eid = request.args.get('eid') 
	if eid is None: 
		abort(404) 
	row = Event.query.get(eid) 
	if row is not None: 
		db.session.delete(row) 
		db.session.commit()
	return "Deleted"

################
# PUT REQUESTS #
################

@app.route('/update_user')
def updateUser(): 
	event_id = request.args.get('eid')
	uid = request.args.get('uid') 
	if uid is None: 
		abort(404) 
	if event_id is not None: 
		user = User.query.get(uid)
		event = Event.query.get(eid) 
		user.events.append(event)
		db.session.commit()
	return "Updated"

@app.route('/update_event', methods = ["POST"])
def updateEvent(): 
	event_id = request.args.get('eid') 
	if event_id is None: 
		abort(404) 
	content = request.json 
	event = Event.query.get(event_id)
	event.name = content["name"],
	event.start = content["start_time"],
	event.end = content["end_time"], 
	event.addr = content["address"],
	event.date = content["date"],
	event.description = content["description"],
	event.type_of_event = content["type"], 
	event.sign_in_url = content["url"]
	db.session.commit()
	return "Updated"





if __name__ == "main": 
	app.run()