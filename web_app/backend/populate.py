from models import User, Event, db
import json

def clear_tables(): 
	db.drop_all() 

def create_tables(): 
	db.create_all()

if __name__ == "__main__": 
	clear_tables() 
	create_tables()